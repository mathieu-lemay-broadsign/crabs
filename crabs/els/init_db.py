from crabs.els import get_client


def insert():
    c = get_client()
    with open("../../data.txt") as f:
        batch = []
        for i, line in enumerate(f):
            id_, t, a, n = line[:-1].split(",")
            batch.append({"index": {"_index": "accounts", "_id": id_}})
            batch.append({"tenant": t, "name": f"{a}-{n}"})

            if i and i % 1000 == 0:
                c.bulk(batch)
                batch = []
                print(i)

        if batch:
            c.bulk(batch)


if __name__ == "__main__":
    # create_table()
    insert()
