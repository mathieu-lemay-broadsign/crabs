import pymongo

from crabs.mongo import get_db


def drop():
    db = get_db()
    db.accounts.drop()


def create_index():
    db = get_db()
    c = db.accounts

    c.create_index("id")
    c.create_index([("tenant", pymongo.ASCENDING), ("name", pymongo.ASCENDING)], unique=True)
    # c.create_index([("name", pymongo.TEXT)])


def insert():
    db = get_db()
    c = db.accounts
    with open("../../data.txt") as f:
        batch = []
        for i, line in enumerate(f):
            id_, t, a, n = line[:-1].split(",")
            batch.append({"id": id_, "tenant": t, "name": f"{a}-{n}"})

            if i and i % 1000 == 0:
                c.insert_many(batch)
                batch = []
                print(i)

        if batch:
            c.insert_many(batch)


def dupes():
    c = get_db().accounts
    # try:
    #     c.insert_many([{"id": '001', "tenant": 'matt', "name": "Matt"}, {"id": '002', "tenant": 'matt', "name": "Matt"}])
    # except Exception as e:
    #     print(e)
    #     raise
    c.insert({"id": '001', "tenant": 'matt', "name": "Matt"})
    c.insert({"id": '001', "tenant": 'matt', "name": "Matt"})


if __name__ == "__main__":
    drop()
    create_index()
    dupes()
    # insert()
