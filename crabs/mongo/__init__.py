from urllib.parse import quote_plus

from pymongo import MongoClient


def get_client(host="127.0.0.1", user="root", passwd="root"):
    uri = f"mongodb://{quote_plus(user)}:{quote_plus(passwd)}@{host}"
    return MongoClient(uri)


def get_db():
    return get_client().account_management
