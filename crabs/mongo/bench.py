from pprint import pprint
from time import time as ts

import pymongo

from crabs.mongo import get_db


def get_by_id(c, id_):
    resp = c.find_one({"id": id_})
    assert resp["id"] == id_


def get_by_tenant(c, tenant, expected_count):
    resp = list(c.find({"tenant": tenant}))
    assert len(resp) == expected_count


def get_by_name(c, tenant, name):
    resp = list(c.find({"tenant": tenant, "name": name}))
    assert len(resp) == 1


def get_for_compl(c, tenant, name):
    resp = list(c.find({"tenant": tenant, "name": {"$regex": f"^{name}"}}))
    # resp = c.find({'tenant': tenant, 'name': {'$text': {'$search': name}}})
    assert len(resp) > 0


def add_ext_data(c, id_, ext_data):
    c.update_one({"id": id_}, {"$set": {"ext_data": ext_data}})


def main():
    ids = (
        "3ff39aab-773a-41d4-90be-1fe832582b94",
        "fb5c5af0-0b0e-4829-bd74-27e3def58641",
        "5eb4b81c-3c64-4fd6-8347-6df25f9d2ecb",
        "1fb05032-8bdb-4426-9270-e07e4ebb60b2",
        "76f37abc-ce1c-4604-98a2-25a2525adc08",
        "bd645c8c-8c76-42c3-aa81-43d8c8c6d008",
        "1092d519-622f-4cd6-b8b5-b0d98ee6c476",
        "ce345d1f-0623-43f6-87c2-40af41ea4170",
        "b32cccef-c31b-41fd-9873-173b9adac59c",
        "2b0e5bb7-e3e1-4ca2-be0a-da7a144f3061",
    )

    tenants = (("exterion_media_ie", 4479), ("ocean_outdoor", 4486))

    names = (
        ("clear_channel_switzerland", "Amazing-Snyder"),
        ("billboard_iceland", "Goofy-Banach"),
        ("on_air_media", "Tender-Swartz"),
        ("branded_cities_us", "Gallant-Noether"),
        ("intersection", "Elated-Borg"),
        ("mass_media_advertizing", "Awesome-Ride"),
        ("ocean_outdoor", "Eloquent-Heyrovsky"),
        ("westfield", "Nifty-Khorana"),
        ("captivate", "Goofy-Mestorf"),
        ("quebecor", "Gallant-Ptolemy"),
    )

    ext_data = {
        "criteria": [],
        "available_only": False,
        "deployed_only": False,
        "dow_mask": 127,
        "multiplier": 14,
        "end_date": "2020-06-22",
        "end_time": "23:59:59",
        "start_date": "2020-06-09",
        "start_time": "00:00:00",
        "inventory_type": "digital",
        "gender_age": [],
        "resolution": [],
        "orientation": [],
        "screen_format": [],
        "category_ids": [],
        "slot_duration": 15,
        "mode": [
            {
                "type": "ppl",
                "active": True,
                "values": {"saturation": 2.0, "bs_saturation": 2.0, "custom_cpm": None},
                "suggested_price": 37110.0,
            },
            {"type": "sov", "active": False, "values": {"sov": 6.25, "custom_cpm": None}},
            {
                "type": "goal_impressions",
                "active": False,
                "values": {"impressions": None, "cpm": None, "budget": None, "custom_cpm": None},
            },
            {
                "type": "goal_budget",
                "active": False,
                "values": {"impressions": None, "cpm": None, "budget": None, "custom_cpm": None},
            },
            {"type": "takeover", "active": False, "values": {"sov": 100.0, "custom_cpm": None}},
        ],
        "keywords": [],
    }

    c = get_db().accounts

    t = ts()
    for id_ in ids:
        get_by_id(c, id_)
    print(f"GetByID: {(ts() - t) * 1000:.3f}ms")

    t = ts()
    for (tenant, expected) in tenants:
        get_by_tenant(c, tenant, expected)
    print(f"GetByTenant: {(ts() - t) * 1000:.3f}ms")

    t = ts()
    for tenant, name in names:
        get_by_name(c, tenant, name)
    print(f"GetByName: {(ts() - t) * 1000:.3f}ms")

    t = ts()
    for tenant, name in names:
        get_for_compl(c, tenant, name.split("-")[0])
    print(f"GetForCompl: {(ts() - t) * 1000:.3f}ms")

    t = ts()
    for id_ in ids:
        add_ext_data(c, id_, ext_data)
    print(f"AddExtData: {(ts() - t) * 1000:.3f}ms")


if __name__ == "__main__":
    main()
