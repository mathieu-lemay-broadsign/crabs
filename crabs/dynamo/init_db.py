from crabs.dynamo import get_client, get_db


def create_table():
    c = get_client()
    try:
        c.create_table(
            TableName="Accounts",
            # Declare your Primary Key in the KeySchema argument
            KeySchema=[{"AttributeName": "tenant", "KeyType": "HASH"}, {"AttributeName": "name", "KeyType": "RANGE"}],
            # Any attributes used in KeySchema or Indexes must be declared in AttributeDefinitions
            AttributeDefinitions=[
                {"AttributeName": "tenant", "AttributeType": "S"},
                {"AttributeName": "name", "AttributeType": "S"},
            ],
            # ProvisionedThroughput controls the amount of data you can read or write to DynamoDB per second.
            # You can control read and write capacity independently.
            ProvisionedThroughput={"ReadCapacityUnits": 1, "WriteCapacityUnits": 1},
        )
        print("Table created successfully!")
    except Exception as e:
        print("Error creating table:")
        print(e)

    c.update_table(
        TableName="Accounts",
        # Any attributes used in our new global secondary index must be declared in AttributeDefinitions
        AttributeDefinitions=[{"AttributeName": "id", "AttributeType": "S"},],
        # This is where we add, update, or delete any global secondary indexes on our table.
        GlobalSecondaryIndexUpdates=[
            {
                "Create": {
                    # You need to name your index and specifically refer to it when using it for queries.
                    "IndexName": "AccountID",
                    # Like the table itself, you need to specify the key schema for an index.
                    # For a global secondary index, you can do a simple or composite key schema.
                    "KeySchema": [{"AttributeName": "id", "KeyType": "HASH"}],
                    # You can choose to copy only specific attributes from the original item into the index.
                    # You might want to copy only a few attributes to save space.
                    "Projection": {"ProjectionType": "ALL"},
                    # Global secondary indexes have read and write capacity separate from the underlying table.
                    "ProvisionedThroughput": {"ReadCapacityUnits": 1, "WriteCapacityUnits": 1,},
                }
            }
        ],
    )
    print("Secondary index added!")


def insert():
    tb = get_db().Table("Accounts")
    with open("../../data.txt") as f:
        with tb.batch_writer() as batch:
            for i, line in enumerate(f):
                id_, t, a, n = line[:-1].split(",")
                batch.put_item(Item={"tenant": t, "name": f"{a}-{n}", "id": id_})

                if i and i % 1000 == 0:
                    print(i)


if __name__ == "__main__":
    # create_table()
    # insert()
    t = get_db().Table("Accounts")

    x = t.put_item(Item={"foo": "bar", "id": 'ffoo'})
    print(x)
