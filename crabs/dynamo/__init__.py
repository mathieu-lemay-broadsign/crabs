import boto3


def get_client():
    return boto3.client("dynamodb", endpoint_url="http://localhost:8000")


def get_db():
    return boto3.resource("dynamodb", endpoint_url="http://localhost:8000")
