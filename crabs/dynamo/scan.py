from time import time as ts

from boto3.dynamodb.conditions import Key

# from . import get_db
#
# db = get_db()
# table = db.Table("Accounts")
#
#
# t = ts()
# resp = table.query(KeyConditionExpression=Key("tenant").eq("outfront") & Key("name").begins_with("Loving"))
# print(f"The query returned {resp['Count']} items in {ts() - t:.3f}s")


def scan():
    scan_kwargs = {
        "FilterExpression": Key("tenant").eq("outfront") & Key("#n").begins_with("Loving"),
        "ProjectionExpression": "#tn, #n, id",
        "ExpressionAttributeNames": {"#tn": "tenant", "#n": "name"},
    }
    f = scan_kwargs['FilterExpression']

    print(str(f))

    items = []

    done = False
    start_key = None
    while not done:
        if start_key:
            scan_kwargs["ExclusiveStartKey"] = start_key
        response = table.scan(**scan_kwargs)
        items += response.get("Items", [])
        start_key = response.get("LastEvaluatedKey", None)
        done = start_key is None

    return items


t = ts()
items = scan()
print(f"The scan returned {len(items)} items in {ts() - t:.3f}s")
