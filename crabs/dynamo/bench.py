from decimal import Decimal
from time import time as ts

from boto3.dynamodb.conditions import Key

from crabs.dynamo import get_db


def get_by_id(table, id_):
    resp = table.query(IndexName="AccountID", KeyConditionExpression=Key("id").eq(id_))
    assert "Items" in resp
    assert len(resp["Items"]) == 1


def get_by_tenant(table, tenant):
    resp = table.query(KeyConditionExpression=Key("tenant").eq(tenant))
    assert "Items" in resp


def get_by_name(table, tenant, name):
    resp = table.get_item(Key={"tenant": tenant, "name": name})
    assert "Item" in resp


def get_for_compl(table, tenant, name):
    resp = table.query(KeyConditionExpression=Key("tenant").eq(tenant) & Key("name").begins_with(name))
    assert "Items" in resp
    assert len(resp["Items"]) > 0


def add_ext_data(table, tenant, name, ext_data):
    table.update_item(
        Key={"tenant": tenant, "name": name},
        ExpressionAttributeValues={":ext_data": ext_data},
        UpdateExpression="SET extra_data = :ext_data",
    )


def main():
    ids = (
        "3ff39aab-773a-41d4-90be-1fe832582b94",
        "fb5c5af0-0b0e-4829-bd74-27e3def58641",
        "5eb4b81c-3c64-4fd6-8347-6df25f9d2ecb",
        "1fb05032-8bdb-4426-9270-e07e4ebb60b2",
        "76f37abc-ce1c-4604-98a2-25a2525adc08",
        "bd645c8c-8c76-42c3-aa81-43d8c8c6d008",
        "1092d519-622f-4cd6-b8b5-b0d98ee6c476",
        "ce345d1f-0623-43f6-87c2-40af41ea4170",
        "b32cccef-c31b-41fd-9873-173b9adac59c",
        "2b0e5bb7-e3e1-4ca2-be0a-da7a144f3061",
    )

    tenants = ("exterion_media_ie", "ocean_outdoor")

    names = (
        ("clear_channel_switzerland", "Amazing-Snyder"),
        ("billboard_iceland", "Goofy-Banach"),
        ("on_air_media", "Tender-Swartz"),
        ("branded_cities_us", "Gallant-Noether"),
        ("intersection", "Elated-Borg"),
        ("mass_media_advertizing", "Awesome-Ride"),
        ("ocean_outdoor", "Eloquent-Heyrovsky"),
        ("westfield", "Nifty-Khorana"),
        ("captivate", "Goofy-Mestorf"),
        ("quebecor", "Gallant-Ptolemy"),
    )

    ext_data = {
        "$top": None,
        "$sort": [{"field": "available_slots", "dir": "desc"}],
        "criteria": [],
        "available_only": False,
        "deployed_only": False,
        "dow_mask": 127,
        "multiplier": 14,
        "end_date": "2020-06-22",
        "end_time": "23:59:59",
        "start_date": "2020-06-09",
        "start_time": "00:00:00",
        "inventory_type": "digital",
        "gender_age": [],
        "resolution": [],
        "orientation": [],
        "screen_format": [],
        "category_ids": [],
        "slot_duration": 15,
        "mode": [
            {
                "type": "ppl",
                "active": True,
                "values": {"saturation": Decimal(2.0), "bs_saturation": Decimal(2.0), "custom_cpm": None},
                "suggested_price": Decimal(37110.0),
            },
            {"type": "sov", "active": False, "values": {"sov": Decimal(6.25), "custom_cpm": None}},
            {
                "type": "goal_impressions",
                "active": False,
                "values": {"impressions": None, "cpm": None, "budget": None, "custom_cpm": None},
            },
            {
                "type": "goal_budget",
                "active": False,
                "values": {"impressions": None, "cpm": None, "budget": None, "custom_cpm": None},
            },
            {"type": "takeover", "active": False, "values": {"sov": Decimal(100.0), "custom_cpm": None}},
        ],
        "keywords": [],
    }

    table = get_db().Table("Accounts")

    t = ts()
    for id_ in ids:
        get_by_id(table, id_)
    print(f"GetByID: {(ts() - t) * 1000:.3f}ms")

    t = ts()
    for tenant in tenants:
        get_by_tenant(table, tenant)
    print(f"GetByTenant: {(ts() - t) * 1000:.3f}ms")

    t = ts()
    for tenant, name in names:
        get_by_name(table, tenant, name)
    print(f"GetByName: {(ts() - t) * 1000:.3f}ms")

    t = ts()
    for tenant, name in names:
        get_for_compl(table, tenant, name.split("-")[0])
    print(f"GetForCompl: {(ts() - t) * 1000:.3f}ms")

    t = ts()
    for tenant, name in names:
        add_ext_data(table, tenant, name, ext_data)
    print(f"AddExtData: {(ts() - t) * 1000:.3f}ms")


if __name__ == "__main__":
    main()
