import json

from . import get_db

db = get_db()
table = db.Table("Accounts")

resp = table.get_item(Key={"tenant": "outfront", "name": "Adoring-Galileo"})
print(json.dumps(resp, indent=2))
resp = table.get_item(Key={"tenant": "fjkkj", "name": "Adoring-Galileo"})
print(json.dumps(resp, indent=2))
