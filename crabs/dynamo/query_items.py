import json
from boto3.dynamodb.conditions import Key

from . import get_db

db = get_db()
table = db.Table("Accounts")

resp = table.query(KeyConditionExpression=Key("tenant").eq("outfront") & Key("name").begins_with("Loving"))

print(f"The query returned the following {len(resp['Items'])} items:")
print(json.dumps(resp, indent=2))
# for item in resp['Items']:
# print(item)
